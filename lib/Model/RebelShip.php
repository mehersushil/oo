<?php

namespace Model;

class RebelShip extends AbstractShip
{
    public function getFavoriteJedi()
    {
        $coolJedis = ['Yoda', 'Ben Kenobi'];
        $key = array_rand($coolJedis);

        return $coolJedis[$key];
    }

    public function getType()
    {
        return 'Rebel';
    }

    public function isFunctional()
    {
        return true;
    }

    public function getJediFactor()
    {
        return rand(10, 30);
    }

    public function getNameAndSpecs($useShortForm = false)
    {
        return parent::getNameAndSpecs($useShortForm) . ' (Rebel)';
    }
}