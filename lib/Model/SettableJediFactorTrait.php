<?php


namespace Model;


trait SettableJediFactorTrait
{
    /**
     * @var int
     */
    private $jediFactor;

    /**
     * @return int
     */
    public function getJediFactor()
    {
        return $this->jediFactor;
    }

    /**
     * @param int $jediFactor
     */
    public function setJediFactor($jediFactor)
    {
        $this->jediFactor = $jediFactor;
    }
}