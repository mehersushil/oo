<?php

namespace Model;

use Exception;

abstract class AbstractShip
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $weaponPower = 0;

    /**
     * @var int
     */
    private $strength = 0;

    public function __construct($name)
    {
        $this->name = $name;
    }

    abstract public function getJediFactor();

    abstract public function getType();

    abstract public function isFunctional();

    public function sayHello()
    {
        echo 'HELLO!';
    }

    /**
     * @param $useShortForm
     * @return string
     */
    public function getNameAndSpecs($useShortForm = false)
    {
        if ($useShortForm) {
            return sprintf(
                '%s: %s/%s/%s',
                $this->name,
                $this->weaponPower,
                $this->getJediFactor(),
                $this->strength
            );
        } else {
            return sprintf(
                '%s: w:%s, j:%s, s:%s',
                $this->name,
                $this->weaponPower,
                $this->getJediFactor(),
                $this->strength
            );
        }
    }

    /**
     * @param Ship $givenShip
     * @return bool
     */
    public function doesGivenShipHaveMoreStrength(Ship $givenShip)
    {
        return $givenShip->strength > $this->strength;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getWeaponPower()
    {
        return $this->weaponPower;
    }

    /**
     * @param int $weaponPower
     */
    public function setWeaponPower($weaponPower)
    {
        $this->weaponPower = $weaponPower;
    }

    /**
     * @return int
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @param int $strength
     * @throws Exception
     */
    public function setStrength($strength)
    {
        if (!is_numeric($strength)) {
            throw new Exception('Invalid strength passed ' . $strength);
        }

        $this->strength = $strength;
    }

    public function __toString()
    {
        return $this->getName();
    }

    // avoid usage of magic method other than __toString and __construct
    /*public function __get($propertyName)
    {
        return $this->$propertyName;
    }*/
}