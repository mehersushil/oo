<?php

namespace Service;

use Exception;
use Model\AbstractShip;
use Model\RebelShip;
use Model\Ship;

class ShipLoader
{
    private $shipStorage;

    public function __construct(ShipStorageInterface $shipStorage)
    {
        $this->shipStorage = $shipStorage;
    }

    /**
     * @param $id
     * @return AbstractShip|null
     * @throws Exception
     */
    public function findOneById($id)
    {
        $shipArray = $this->shipStorage->fetchSingleShipData($id);

        return $this->createShipFromData($shipArray);
    }

    /**
     * @param $shipData
     * @return Ship
     * @throws Exception
     */
    private function createShipFromData($shipData)
    {
        if ($shipData['team'] === 'rebel') {
            $ship = new RebelShip($shipData['name']);
        } else {
            $ship = new Ship($shipData['name']);
            $ship->setJediFactor($shipData['jedi_factor']);
        }

        $ship->setId($shipData['id']);
        $ship->setWeaponPower($shipData['weapon_power']);
        $ship->setStrength($shipData['strength']);

        return $ship;
    }

    /**
     * @return AbstractShip[]
     * @throws Exception
     */
    public function getShips()
    {
        $shipsData = [];
        $ships = [];

        try {
            $shipsData = $this->shipStorage->fetchAllShipsData();
        } catch (Exception $e) {
            trigger_error('Exception: ' . $e->getMessage());
            $ships = [];
        }

        foreach ($shipsData as $shipData) {
            $ships[] = $this->createShipFromData($shipData);
        }

        return $ships;
    }
}