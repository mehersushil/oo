<?php

use Model\Ship;

/**
 * @param Ship $someShip
 */
function printShipSummary(Ship $someShip)
{
    echo 'Ship Name: ' . $someShip->getName();
    echo '<hr/>';
    $someShip->sayHello();
    echo '<hr/>';
    echo $someShip->getName();
    echo '<hr/>';
    echo $someShip->getNameAndSpecs(false);
    echo '<hr/>';
    echo $someShip->getNameAndSpecs(true);
}

$myShip = new Ship('Jedi Starship');
$myShip->setweaponPower(10);

$otherShip = new Ship('Imperial Shuttle');
$otherShip->setWeaponPower(5);
$otherShip->setStrength(50);

printShipSummary($myShip);
echo '<hr/>';
printShipSummary($otherShip);
echo '<hr/>';

if ($myShip->doesGivenShipHaveMoreStrength($otherShip)) {
    echo $otherShip->getName() . ' has more strength.';
} else {
    echo $myShip->getName() . ' has more strength.';
}